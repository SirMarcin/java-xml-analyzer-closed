package xml_analyzer;

import java.util.HashMap;
import java.util.Map.Entry;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class SAXFile extends DefaultHandler {

   private HashMap<String, Integer> elements = new HashMap<>();
   
   private boolean elementFound = false;
   private int elementCount = 0;
   
   @Override
   public void startElement(String uri, 
      String localName, String qName, Attributes attributes)
         throws SAXException {
       
        //System.out.println(qName);
       
         for(Entry<String, Integer> en : elements.entrySet()) {
                if (en.getKey().equalsIgnoreCase(qName)) {
                    elementFound = true;
                    elementCount = en.getValue();
                    break;
                }
            }
            
            if (!elementFound) {
                elements.put(qName, 1);
                //System.out.println(qName);
            } else {
                elements.replace(qName, ++elementCount);
            }
            elementFound = false;
            
        
      
//      if (qName.equalsIgnoreCase("student")) {
//         String rollNo = attributes.getValue("rollno");
//         System.out.println("Roll No : " + rollNo);
//      } else if (qName.equalsIgnoreCase("firstname")) {
//         bFirstName = true;
//      } else if (qName.equalsIgnoreCase("lastname")) {
//         bLastName = true;
//      } else if (qName.equalsIgnoreCase("nickname")) {
//         bNickName = true;
//      }
//      else if (qName.equalsIgnoreCase("marks")) {
//         bMarks = true;
//      }
   }

   @Override
   public void endElement(String uri, 
      String localName, String qName) throws SAXException {
      //System.out.println("End Element :" + qName);
      
   }

   @Override
   public void characters(char ch[], 
      int start, int length) throws SAXException {
       
       //System.out.println("Content: " + new String(ch, start, length));
//       
//      if (bFirstName) {
//         System.out.println("First Name: " 
//         + new String(ch, start, length));
//         bFirstName = false;
//      } else if (bLastName) {
//         System.out.println("Last Name: " 
//         + new String(ch, start, length));
//         bLastName = false;
//      } else if (bNickName) {
//         System.out.println("Nick Name: " 
//         + new String(ch, start, length));
//         bNickName = false;
//      } else if (bMarks) {
//         System.out.println("Marks: " 
//         + new String(ch, start, length));
//         bMarks = false;
//      }
   }
   
   public HashMap<String, Integer> getElementsMap(){
       return elements;
   }
   
}
