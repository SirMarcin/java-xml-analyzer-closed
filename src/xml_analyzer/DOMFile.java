package xml_analyzer;

import java.io.File;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;

/**
 *
 * @author Marcin
 */
public class DOMFile {
    
    private File file = null;
    
    public DOMFile(){
        DocumentBuilderFactory documentBuilder = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = null;
        
        try{
            builder = documentBuilder.newDocumentBuilder();
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        try {
            Document document = builder.parse(file);
        } catch (Exception e) {
            e.printStackTrace();
        }
        
    }
}
