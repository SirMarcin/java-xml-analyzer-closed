package xml_analyzer;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Stream;

public class XmlPlainText {
    
    private String fileName;
    
    public XmlPlainText(String f){
        fileName = f; //"c:\\Users\\mcmurawski\\Documents\\NetBeansProjects\\XML_analyzer\\_testFiles\\1539-CNT-muni_ww-Nereda_App_note-email_(fr-be).xlf"; //JOptionPane.showInputDialog("File path");
    }
    
    public List<String> readFile(){
        
                
        List <File> files = new ArrayList<File>();
        List <BufferedReader> readers = new ArrayList<BufferedReader>();
        List <String> fileLines = new ArrayList<>();
        
        files.add(new File(fileName));
        
        try {
            readers.add(new BufferedReader(new FileReader(files.get(0))));
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(XML_analyzer.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Error creating buffered reader for file");
        }
        
        try {
            Stream <String> content = readers.get(0).lines();
            
            Object[] linie = content.toArray();
            
            for (Object linia : linie) fileLines.add(linia.toString());
            
            
        } catch (Exception e) {
            System.out.println("Error reading line from file");
        }
        return fileLines;
    }
}
