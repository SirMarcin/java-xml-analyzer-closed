package xml_analyzer;

import java.awt.datatransfer.DataFlavor;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DnDConstants;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.DefaultListModel;
import javax.swing.JTextArea;
import javax.swing.ListModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

/**
 *
 * @author mcmurawski
 */
public class MainWindow extends javax.swing.JFrame {
    
    private List<File> fileList = new ArrayList<>();
    DefaultTableModel fileListModel;
    /**
     * Creates new form MainWindow
     */
    public MainWindow() {
        initComponents();
        
        
        jTextArea.setEditable(false);
        
        fileListModel = new DefaultTableModel();
        fileListModel.addColumn("File name");
        fileListModel.addColumn("Comment");
        fileListModel.setRowCount(6);
        
        jButton1.setContentAreaFilled(false);
        jParseButton.setContentAreaFilled(false);
        
        jTable1.setModel(fileListModel);
        
        // handle drag&drop for files
        jTable1.setDropTarget(new DropTarget() {
            @Override
            public synchronized void drop(DropTargetDropEvent evt) {
                try {
                    evt.acceptDrop(DnDConstants.ACTION_COPY);
                    fileList = (List<File>)evt.getTransferable().getTransferData(DataFlavor.javaFileListFlavor);
                    fileListModel.setRowCount(fileList.size());
                    updateTable(jTable1, fileListModel);
                    int tableIndex = 0;
                    for (File file : fileList) {
                        System.out.println(file.getName());
                        jTable1.setValueAt(file.getName(), tableIndex++, 0);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                } 
            }
        });
        
        
        // nasłuchiwanie kliknięć w wiersze tabeli
        //
        // TO DO: póki co wywołuje funkcję przy kliknięciu i odkliknięciu - trzeba to zmienić
        //
        jTable1.getSelectionModel().addListSelectionListener(new ListSelectionListener(){

            @Override
            public void valueChanged(ListSelectionEvent e) {
                System.out.println(jTable1.getValueAt(jTable1.getSelectedRow(), jTable1.getSelectedColumn()));
                
            }
            
        });
    }
    
    private String parseQuick(String filePath) throws IOException{
        String parseMsg = "";
        
        try{
        XMLReader reader = XMLReaderFactory.createXMLReader();
        reader.setFeature("http://xml.org/sax/features/validation", true);
        
        ErrorHandler handler = new FullErrorHandler();
        
        //reader.setErrorHandler(handler);
        
        reader.parse(filePath);
        
        
        } catch (SAXException e){
        
        }
        return "";
    }
    private String parseXML(String filePath){
        String parseMsg = "";
        
        HashMap<String, Integer> elements = new HashMap<>();
        SAXFile userHandler = new SAXFile();
        boolean isValid = true;
        
        try {
            File plik = new File(filePath);
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser saxParser = factory.newSAXParser();
            

            saxParser.parse(plik, userHandler);
            
            SAXFile test = new SAXFile();
        } catch (Exception e){
            System.out.println("File is not valid!\n");
            parseMsg += "File is not valid!\n\n" + e.toString() + "\n";
            System.out.println(e.toString() + "\n");
            isValid = false;
            
            
        }
        
        elements = userHandler.getElementsMap();
        
        if (isValid) {
            System.out.println("File is valid\n");
            parseMsg += "File is valid\n";
        }
        
        
//        System.out.println("=== XML elements ===");
//        for(Map.Entry<String, Integer> en : elements.entrySet()) {
//                System.out.println(en.getValue() + "\t" + en.getKey());
//                //parseMsg += en.getValue() + "\t" + en.getKey() + "\n";
//            }
        
        return parseMsg;
    }
    private String getXMLElements(String filePath){
        String parseMsg = "";
        
        HashMap<String, Integer> elements = new HashMap<>();
        SAXFile userHandler = new SAXFile();
        boolean isValid = true;
        
        try {
            File plik = new File(filePath);
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser saxParser = factory.newSAXParser();
            

            saxParser.parse(plik, userHandler);
            
            SAXFile test = new SAXFile();
        } catch (Exception e){
            System.out.println("File is not valid!\n");
            parseMsg += "File is not valid!\n\n" + e.toString() + "\n";
            System.out.println(e.toString() + "\n");
            isValid = false;
            
        }
        
        elements = userHandler.getElementsMap();
        if (isValid){
            System.out.println("=== XML elements ===");
            parseMsg += "=== XML elements ===\n";
            for(Map.Entry<String, Integer> en : elements.entrySet()) {
                    System.out.println(en.getValue() + "\t" + en.getKey());
                    parseMsg += en.getValue() + "\t" + en.getKey() + "\n";
                }
        }
        return parseMsg;
    }
    private String getFileLine(String filePath, int lineNumber) {
        List<String> allLines;
        String line = "";
        
        XmlPlainText file = new XmlPlainText(filePath);
        allLines = file.readFile();
        line = allLines.get(lineNumber);
        
        return line;
    }
    private void updateTable(javax.swing.JTable table, DefaultTableModel model){
        table.setModel(model);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTextArea = new javax.swing.JTextArea();
        jParseButton = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        jTextArea.setColumns(20);
        jTextArea.setRows(5);
        jScrollPane1.setViewportView(jTextArea);

        jParseButton.setText("Parse | SAX");
        jParseButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jParseButtonMouseClicked(evt);
            }
        });

        jButton1.setText("Get elements");
        jButton1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButton1MouseClicked(evt);
            }
        });

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null}
            },
            new String [] {
                "File name", "Comment"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane2.setViewportView(jTable1);

        jButton2.setText("Parse | DOM");

        jButton3.setText("Quick Parse");
        jButton3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButton3MouseClicked(evt);
            }
        });

        jMenu1.setText("File");

        jMenuItem1.setText("Open");
        jMenu1.add(jMenuItem1);

        jMenuItem2.setText("Quit");
        jMenu1.add(jMenuItem2);

        jMenuBar1.add(jMenu1);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(35, 35, 35)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jParseButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButton1))
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 648, Short.MAX_VALUE)
                    .addComponent(jScrollPane1))
                .addGap(37, 37, 37))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jParseButton, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButton1, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
                    .addComponent(jButton2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButton3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 120, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 177, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jParseButtonMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jParseButtonMouseClicked
        String parseMsg = "";
        // clear text area
        jTextArea.setText(null);
        
        //if (!fileList.isEmpty()) {
            for (File file : fileList) {
                parseMsg = parseXML(file.getAbsolutePath());
                String[] parseMsgSplit = parseMsg.split(";[^']");
                int lineNumber, columnNumber;
                String errorDescription;

                // print parsing result and format it depending on content
                if (parseMsg.substring(0, 13).equals("File is valid")) {
                    jTextArea.append(parseMsg);
                    jTable1.setValueAt("valid", fileList.indexOf(file), 1);
                    
                }
                else if (parseMsgSplit.length == 5) {
                    // parse error input and split it into separate variables
                    lineNumber = Integer.parseInt(parseMsgSplit[2].split(":")[1].trim()) - 1;
                    columnNumber = Integer.parseInt(parseMsgSplit[3].split(":")[1].trim()) - 1;
                    errorDescription = parseMsgSplit[4];
                    
                    
                    jTable1.setValueAt(errorDescription, fileList.indexOf(file), 1);
                    jTextArea.append("\n\nFile is not valid!\n\n" + "Error details: " + errorDescription + "Position: " + columnNumber + "\nLine: " + lineNumber + "\n\n");

                }
                else {
                    jTextArea.append(parseMsg);
                    jTable1.setValueAt("not valid, click for more details", fileList.indexOf(file), 1);
                }
            }
            
            //updateTable(jTable1, fileListModel);
        //}
    }//GEN-LAST:event_jParseButtonMouseClicked

    private void jButton1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton1MouseClicked
//          TO DO
        jTextArea.setText(null);
        
        for (File file : fileList) {
            jTextArea.setText(getXMLElements(file.getAbsolutePath()));
        }
        
    }//GEN-LAST:event_jButton1MouseClicked

    private void jButton3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton3MouseClicked
        String parseMsg = "";
        
        jTextArea.setText("");
        
        for (File file: fileList) {
            try {
            parseQuick(file.getAbsolutePath());
            } catch (IOException e) {
                
            }
        }
    }//GEN-LAST:event_jButton3MouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MainWindow().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JButton jParseButton;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable jTable1;
    private javax.swing.JTextArea jTextArea;
    // End of variables declaration//GEN-END:variables
}
